//
//  ViewController.m
//  SZKitDemo
//
//  Created by Song Zhou on 26/11/2017.
//  Copyright © 2017 Song Zhou. All rights reserved.
//

#import "ViewController.h"
#import "SZGlobal.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor colorFromHex:0xE41B17];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
